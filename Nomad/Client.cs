﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nomad
{
    public class Client : Entity
    {
        public string Login { get; set; }
        public string Password { get; set; }
        public bool Subscription { get; set; } = false;
        public int SubMonth { get; set; } = 0;
    }
}
