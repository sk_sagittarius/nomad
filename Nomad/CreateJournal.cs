﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nomad
{
    public class CreateJournal
    {
        int i = 1;

        public void Create()
        {
            Console.WriteLine("Создать журнал (Y/N)");
            var answer = Console.ReadLine().ToLower();
            Console.WriteLine("Журнал с номером " + i + " выпущен!");
            i++; 
        }
    }
}
