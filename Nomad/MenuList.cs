﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nomad
{
    public class MenuList
    {
        //в меню еще много косяков, лучше проверять только админ, там чуть почище
        Registration registration = new Registration();
        SignIn signIn = new SignIn();
        CreateJournal createJournal = new CreateJournal();
        Subscriptions subscriptions = new Subscriptions();

        public void Menu()
        {
            Console.WriteLine("Админ или клиент");
            Console.WriteLine("{0}. {1}", 1, "Админ");
            Console.WriteLine("{0}. {1}", 2, "Клиент");
            int key = int.Parse(Console.ReadLine());
            switch (key)
            {
                case 1:
                    Console.WriteLine("Выберите функцию");
                    Console.WriteLine("{0}. {1}", 1, "Регистрация");
                    Console.WriteLine("{0}. {1}", 2, "Авторизация");
                    Console.WriteLine("{0}. {1}", 3, "Создать журнал");
                    Console.WriteLine("{0}. {1}", 4, "Посчитать доход на данный момент");
                    Console.WriteLine("{0}. {1}", 5, "Вернуться в главное меню");
                    key = int.Parse(Console.ReadLine());
                    Console.Clear();
                    switch (key)
                    {
                        case 1:
                            registration.Add();
                            System.Threading.Thread.Sleep(1000);
                            Console.Clear();
                            Menu();
                            break;
                        case 2:
                            Console.WriteLine("Login");
                            var login = Console.ReadLine();
                            signIn.Sign(login);
                            break;
                        case 3: 
                            createJournal.Create();
                            System.Threading.Thread.Sleep(5000);
                            Console.Clear();
                            Menu();
                            break;
                        case 4:
                            subscriptions.Amount();
                            Console.WriteLine("Данные занесены в таблицу!");
                            System.Threading.Thread.Sleep(1000);
                            Console.Clear();
                            Menu();
                            break;
                        case 5: Menu();
                            break;
                        default: break;
                    }
                    break;
                case 2:
                    Console.WriteLine("Выберите функцию");
                    Console.WriteLine("{0}. {1}", 1, "Регистрация");
                    Console.WriteLine("{0}. {1}", 2, "Авторизация");
                    Console.WriteLine("{0}. {1}", 3, "Вернуться в главное меню");
                    key = int.Parse(Console.ReadLine());
                    switch (key)
                    {
                        case 1:
                            registration.Add();
                            break;
                        case 2:
                            Console.WriteLine("Login");
                            var login = Console.ReadLine();
                            signIn.Sign(login);
                            break;
                        case 3: Menu();
                            break;
                        default: break;

                    }
                    break;
                default:break;
            }
        }
    }
}
