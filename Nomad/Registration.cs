﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nomad
{
    public class Registration
    {
        public void Add ()
        {
            using (var context = new NomadContext())
            {
                Client client = new Client();
                Console.WriteLine("Login");
                client.Login = Console.ReadLine().ToLower();
                if (context.Clients.Any(i => i.Login == client.Login))
                {
                    Console.WriteLine("Login is not free");
                }
                else
                {
                    Console.WriteLine("Password");
                    client.Password = Console.ReadLine();
                    context.Clients.Add(client);
                    context.SaveChanges();
                }
            }
        }
    }
}
