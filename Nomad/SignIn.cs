﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nomad
{
    public class SignIn
    {
        public void Sign(string login)
        {
            using (var context = new NomadContext())
            {
                MenuList menuList = new MenuList();

                Subscriptions subscriptions = new Subscriptions();
                Client client = new Client();
                if (context.Clients.Any(i => i.Login == login))
                {
                    Console.WriteLine("Password");
                    var password = Console.ReadLine();
                    if (context.Clients.Where(user => user.Login == login).ToList().ElementAt(0).Password == password)
                    {
                        Console.WriteLine("Sign in!");
                        System.Threading.Thread.Sleep(1000);
                        Console.Clear();
                        if (context.Clients.Where(user => user.Login == login).ToList().ElementAt(0).Subscription == false)
                        {
                            Console.WriteLine("Купить подписку (Y/N)");
                            var answer = Console.ReadLine().ToLower();
                            if (answer == "y")
                            {
                                Console.WriteLine("Выберите период (12/24/36)");
                                Console.WriteLine("{0}. {1}", 1, "12 месяцев");
                                Console.WriteLine("{0}. {1}", 2, "24 месяца");
                                Console.WriteLine("{0}. {1}", 3, "36 месяцев");

                                int subMonth = int.Parse(Console.ReadLine());
                                if(subMonth==12 || subMonth==24 || subMonth==36)
                                {
                                    Console.WriteLine("Заявка обрабатывается, пожалуйста подождите");
                                    System.Threading.Thread.Sleep(5000);
                                    if (subMonth == 12 || subMonth == 36)
                                    {
                                        Console.WriteLine("Подписка оформлена на " + subMonth + " месяцев");
                                    }
                                    else
                                    {
                                        Console.WriteLine("Подписка оформлена на " + subMonth + " месяца");
                                    }
                                    context.Clients.Where(user => user.Login == login).ToList().ElementAt(0).Subscription = true;
                                    context.Clients.Where(user => user.Login == login).ToList().ElementAt(0).SubMonth = subMonth;
                                    context.SaveChanges();
                                    System.Threading.Thread.Sleep(1000);
                                    Console.Clear();
                                    menuList.Menu();
                                }
                                else
                                {
                                    Console.WriteLine("Wrong answer");
                                }
                            }
                            else
                            {
                                Console.WriteLine("OK!");
                            }
                        }
                        else if (context.Clients.Where(user => user.Login == login).ToList().ElementAt(0).Subscription == true)
                        {
                            Console.WriteLine("Аннулировать подписку (Y/N)");
                            var answer = Console.ReadLine().ToLower();
                            if (answer == "y")
                            {
                                Console.WriteLine("Заявка обрабатывается, пожалуйста подождите");
                                System.Threading.Thread.Sleep(5000);
                                Console.WriteLine("Подписка аннулирована!");
                                subscriptions.Amount(context.Clients.Where(user => user.Login == login).ToList().ElementAt(0).SubMonth);
                                context.Clients.Where(user => user.Login == login).ToList().ElementAt(0).Subscription = false;
                                context.Clients.Where(user => user.Login == login).ToList().ElementAt(0).SubMonth = 0;
                                context.SaveChanges();
                                System.Threading.Thread.Sleep(1000);
                                Console.Clear();
                                menuList.Menu();

                            }
                            else
                            {
                                Console.WriteLine("OK!");
                            }
                        }
                    }
                    else
                    {
                        Console.WriteLine("Error password!");
                    }
                }
                else
                {
                    Console.WriteLine("Error login!");
                }
            }
        }
    }
}
