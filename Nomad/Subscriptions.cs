﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nomad
{
    public class Subscriptions
    {
        public void Amount(int afterCancellation = 0)
        {
            using (var context = new NomadContext())
            {
                FinJournal finJournal = new FinJournal();
                int monthPrice = 15000;
                var clientCount = context.Clients.Count();
                //int yearProfit=0;
                int monthProfit = 0;
                int month = 1;
                for (int i = 0; i < context.Clients.Count(); i++)
                {
                    if (context.Clients.ToList().ElementAt(i).Subscription == true)
                    {
                        //yearProfit += context.Clients.ToList().ElementAt(i).SubMonth * monthPrice;
                        monthProfit += monthPrice;
                    }

                }
                //Console.WriteLine(yearProfit+afterCancellation*monthPrice);
                finJournal.Month = month;
                month++; // не добавляет значения, поэтому каждый месяц в таблице будет 1
                finJournal.Amount = monthProfit;
                context.FinJournals.Add(finJournal);
                context.SaveChanges();

            }
        }
    }
}
